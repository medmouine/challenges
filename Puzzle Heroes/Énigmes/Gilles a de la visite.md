# Puzzle Heroes

## Gilles reçoit de la visite

### Mise en situation

Gilles reçoit de la visite pour souper. Il a préparé une délicieuse lasagne. Son cousin Paul le mathématicien arrive avec une douzaine de caisses de 24 oeufs.

**Gilles**: Paul! Quel plaisir de te voir! Tu te souviens de mes trois chiens Pirouette, Coconut et Claude?

**Paul**: Gilles, ça faisait si longtemps! Wow, je vois que tu as encore tes animaux depuis la dernière fois! Mais ça fait combien de temps que tu les as dis donc?

**Gilles**: Oui, et ils sont encore en santé! Je te propose un petit jeu pour deviner leurs âges afin de détendre l'atmosphère.

**Paul**: Vas-y.

(L'ambiance était déjà à son comble)

**Gilles**:

* Le produit de l'âge des trois chiens est de 36.

**Paul**: ... je ne sais pas.

**Gilles**:

* La somme de leurs âges est égale au nombre de minutes sur l'horloge dans exactement 5 minutes.

**Paul**: ... c'est difficile à dire.

**Gilles**:

* Mon plus vieux chien préfère les croquettes de poulet aux croquettes de poisson.

**Paul**: AH! C'est facile j'ai trouvé!

### Instructions
Vous devez trouver l'âge des trois chiens de Gilles. Écrivez votre démarche et votre réponse dans ce README.

### Indice
Aucun détail de la mise en scène n'est superflu.

### Réponse
Écrivez votre réponse ici:
